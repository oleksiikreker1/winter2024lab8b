public enum Tile {
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("H"),
	CASTLE("C");
	
	private final String name;
	
	//Constructor
	private Tile(String name) {
		this.name = name;
	}
	
	//Getter
	public String getName() {
		return this.name;
	}
} 