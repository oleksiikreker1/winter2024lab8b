import java.util.Random;

public class Board {
	private Tile[][] grid;
	private final int gridSize = 5;
	
	//Constructor
	public Board() {
		grid = new Tile[gridSize][gridSize];
		
		Random rand = new Random();
		
		for(int i = 0; i < gridSize; i++) {
			
			int randIndex = rand.nextInt(grid[i].length);
			
			for(int j = 0; j < gridSize; j++) {
				
				if(j == randIndex) {
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else {
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	public int placeToken(int row, int col) {
		if(grid[col][row] == Tile.BLANK) {
			grid[col][row] = Tile.CASTLE;
			return 0;
		}
		else if(grid[col][row] == Tile.HIDDEN_WALL) {
			grid[col][row] = Tile.WALL;
			return 1;
		}
		else if(grid[col][row] == Tile.CASTLE || grid[col][row] == Tile.WALL) {
			return -1;
		}
		else {
			return -2;
		}
	}
	
	public String toString() {
		String message = "";
		int gridLength = this.grid.length;
		for(int i = 0; i < gridLength; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				message += (this.grid[i][j].getName() + " ");
			}
			message += "\n";
		}
		return message;
	}
}