import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int numCastles = 7;
		int turns = 0;
		Board board = new Board();
		
		System.out.println("Welcome!");
		
		while(numCastles > 0 && turns < 8) {
			System.out.println(board);
			System.out.println("Number of castles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			
			System.out.println("Enter a number to represent a column (Between 0 and 4): ");
			int col = Integer.parseInt(reader.nextLine());
			System.out.println("Enter a number to represent a row (Between 0 and 4): ");
			int row = Integer.parseInt(reader.nextLine());
			
			int points = board.placeToken(col, row);
			
			while(points < 0) {
				System.out.println("Enter a number to represent a column (Between 0 and 4): ");
				col = Integer.parseInt(reader.nextLine());
				System.out.println("Enter a number to represent a row (Between 0 and 4): ");
				row = Integer.parseInt(reader.nextLine());
				
				points = board.placeToken(col, row);
			}
			
			if(points == 1) {
				System.out.println("There was a wall at that position");
			}
			else {
				System.out.println("A castle tile was successfully placed");
				numCastles--;
			}
			turns++;
		}
		
		System.out.println(board);
		
		if(numCastles == 0) {
			System.out.println("You won!");
		}
		else {
			System.out.println("You lost!");
		}
	}
}